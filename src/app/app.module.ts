import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { AngularFireModule } from 'angularfire2';

import { MyApp } from './app.component';
import { HomePage } from './../pages/home/home';
import { Login } from './../pages/login/login';
import { RegisterModule } from './../pages/register/register.module';
import { ForgotPasswordModule } from './../pages/forgot-password/forgot-password.module';
import { ProfileModule } from './../pages/profile/profile.module';
import { ChatModule } from './../pages/chat/chat.module';
import { Authprovider } from './../providers/authprovider';
import { TokenManager } from './../providers/token-manager';
import { Doctorprovider } from './../providers/doctorprovider';

export const firebaseConfig = {
  apiKey: "F6X8Mg7qIBlHYaIT5pl3VtNTAYnfj9DWTYbM9Lz7",
  authDomain: "https://fcm.googleapis.com/fcm/send",
  databaseURL: "https://docster-dev-9e23f.firebaseio.com"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Login
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RegisterModule,
    ForgotPasswordModule,
    ProfileModule,
    ChatModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Login
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Authprovider,
    TokenManager,
    Doctorprovider
  ]
})
export class AppModule {}
