import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

/*
  Generated class for the Doctorprovider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Doctorprovider {

  baseUrl = "http://localhost:3000/";
  secondaryUrl = "api/doctors/v1/details/";

  constructor(public http: Http) {
    console.log('Hello Doctorprovider Provider');
  }

  private setHeader(token: string) : Headers {
    let headerConfig = {
      Authorization : `Bearer ${token}`,
      'Content-Type': 'application/json'
    }

    return new Headers(headerConfig)

  }

  // validateUser(path) {
  //   let token = localStorage.getItem('auth_token');
  //   console.log(token);

  //   this.http.get()
  //   // this.setHeader(token);

  // }

  getSpecialisations(path:string) : Observable<any> {
    return this.http.get(`${ this.baseUrl }${ this.secondaryUrl }${ path }`).map((res:Response) => {
        // If status is not 200, throw error else send response
        if (res.status < 200 || res.status >= 300) {
          throw new Error("Something went wrong" + res.status);
        } else {
          return res.json();
        }
    });
  }

  getCities(path:string) : Observable<any> {
    return this.http.get(`${ this.baseUrl }${ this.secondaryUrl }${ path }`).map((res:Response) => {
        // If status is not 200, throw error else send response
        if (res.status < 200 || res.status >= 300) {
          throw new Error("Something went wrong" + res.status);
        } else {
          return res.json();
        }
    });
  }

  getQualifications(path:string) : Observable<any> {
    return this.http.get(`${ this.baseUrl }${ this.secondaryUrl }${ path }`).map((res:Response) => {
        // If status is not 200, throw error else send response
        if (res.status < 200 || res.status >= 300) {
          throw new Error("Something went wrong" + res.status);
        } else {
          return res.json();
        }
    });
  }

  getProfile(path: string, token: string) : Observable<any> {
    return this.http.get(`${this.baseUrl}${path}`, {headers: this.setHeader( token )}).map((res:Response) => {
        // If status is not 200, throw error else send response
        if (res.status < 200 || res.status >= 300) {
          throw new Error("Something went wrong" + res.status);
        } else {
          return res.json();
        }
    });
  }

  updateProfile(path: string, token: string, body) {
    return this.http.put(`${this.baseUrl}${path}`, JSON.stringify(body), {headers: this.setHeader( token )}).map((res:Response) => {
        // If status is not 200, throw error else send response
        if (res.status < 200 || res.status >= 300) {
          throw new Error("Something went wrong" + res.status);
        } else {
          return res.json();
        }
    });
  }

}
