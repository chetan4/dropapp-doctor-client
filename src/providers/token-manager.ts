import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';

/*
  Generated class for the TokenManager provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class TokenManager {

  private token: string;

  constructor(public http: Http) {

  }

  store_token(token) {
    localStorage.setItem("auth_token", token);
  }

  check_token() {
    this.token = localStorage.getItem("auth_token");
    if (!this.token) {
      return Observable.create(observer => {
        observer.next(false);
        observer.complete();
      });
    } else {
        return Observable.create(observer => {
          observer.next(true);
          observer.complete();
      });
    }
  }

}
