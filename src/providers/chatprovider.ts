import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';

/*
  Generated class for the ChatChat provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ChatService {

  public chatUrl = 'https://docster-dev-9e23f.firebaseio.com';
  public localhost = "http://localhost:3000";

  constructor(public http: Http) {
    console.log('Hello Chat Provider');
  }

  private setHeader(token: string) : Headers {
    let headerConfig = {
      Authorization : `Bearer ${token}`,
      'Content-Type': 'application/json'
    }

    return new Headers(headerConfig)

  }

  query(token: string) {
    return this.http.get(`${this.chatUrl}/chats`, {headers: this.setHeader( token )}).map((res) => {
      res.json();
    });

  }

  query_by_patient_id(token: string, patient_id: number): Observable<any> {
    return this.http.get(`${this.chatUrl}api/doctors/v1/chats?chat[patient_id]=${patient_id}`, {headers: this.setHeader( token )}).map((res) => {
      return res.json();
    });

  }

  create(token: string, body: any): Observable<any> {
    return this.http.post(`${this.localhost}/api/doctors/v1/chats`, JSON.stringify(body), {headers: this.setHeader( token )}).map((res:Response) => {
      return res.json();
    });
  }

}
