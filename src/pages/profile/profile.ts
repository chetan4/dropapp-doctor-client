
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Authprovider } from './../../providers/authprovider';
import { Doctorprovider } from './../../providers/doctorprovider';

import { HomePage } from './../home/home';

/**
 * Generated class for the Profile page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class Profile {

  update_profile_path: string = "doctors/profile";
  cities: any[] = [];
  specialisationIds: any[] = [];
  qualificationIds: any[] = [];
  cities_path: string =  "city_list";
  specialisation_path: string =  "specialisation_list";
  qualification_path: string =  "qualification_list";

  profile_form: FormGroup;
  email_regex = /^(?:(?:[\w`~!#$%^&*\-=+;:{}'|,?\/]+(?:(?:\.(?:"(?:\\?[\w`~!#$%^&*\-=+;:{}'|,?\/\.()<>\[\] @]|\\"|\\\\)*"|[\w`~!#$%^&*\-=+;:{}'|,?\/]+))*\.[\w`~!#$%^&*\-=+;:{}'|,?\/]+)?)|(?:"(?:\\?[\w`~!#$%^&*\-=+;:{}'|,?\/\.()<>\[\] @]|\\"|\\\\)+"))@(?:[a-zA-Z\d\-]+(?:\.[a-zA-Z\d\-]+)*|\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\])$/;

  phone_id: any;

  user = {
    doctor: {
      'first_name': null,
      'last_name' : null,
      'city_id': null,
      'phone_attributes' : {'id': null , 'number': null},
      'gender' : null,
      'specialisation_ids' : [],
      'qualification_ids': []
    }
  };

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private fb: FormBuilder,
              private authProvider: Authprovider,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController,
              private dp: Doctorprovider) {

   this.profile_form = fb.group({
      // This will validate the input fields
      first_name: [null, Validators.required],
      last_name: [null, Validators.required],
      number: [null, Validators.required],
      city_id: [null, Validators.required],
      gender: [null, Validators.required],
      specialisation_ids: [null, Validators.required],
      qualification_ids: [null, Validators.required]
    });

  }

  get_data(form) {
    this.user.doctor["first_name"] = form.first_name;
    this.user.doctor["last_name"] = form.last_name;
    this.user.doctor.phone_attributes = { id: this.phone_id, number: form.number };
    this.user.doctor["gender"] = form.gender;
    this.user.doctor["city_id"] = form.city_id;
    this.user.doctor["specialisation_ids"] = form.specialisation_ids;
    this.user.doctor["qualification_ids"] = form.qualification_ids;

    console.log(JSON.stringify(this.user));
    let token = localStorage.getItem('auth_token');
    this.dp.updateProfile(this.update_profile_path, token, this.user ).subscribe((res) => {
      this.navCtrl.pop(HomePage);
    });

  }

  get_cities() {
    this.dp.getCities(this.cities_path).subscribe((res) => {
      this.cities = res.cities;
    });

  }

  get_specialisation_ids() {
    this.dp.getSpecialisations(this.specialisation_path).subscribe((res) => {
      this.specialisationIds = res.specialisation;
    });

  }

  get_qualification_ids() {
    this.dp.getQualifications(this.qualification_path).subscribe((res) => {
      if(res.success === true) {
        this.qualificationIds = res.qualifications;
      }
    });

  }

  ionViewDidLoad() {
    let s_ids = [],
        qualification_ids = [];
    this.get_cities();
    this.get_specialisation_ids();
    this.get_qualification_ids();
    this.navParams.data.doctor.doctor_specialisations.map((data) => {
        s_ids.push(data.id);
    });
    this.navParams.data.doctor.doctor_qualifications.map((data) => {
      qualification_ids.push(data.id);
    });
    this.phone_id = this.navParams.data.doctor.phone.id;

    this.profile_form.setValue({
        first_name: this.navParams.data.doctor.first_name,
        last_name: this.navParams.data.doctor.last_name,
        city_id: this.navParams.data.doctor.city.id,
        number: this.navParams.data.doctor.phone.number,
        gender: this.navParams.data.doctor.gender,
        specialisation_ids: s_ids,
        qualification_ids: qualification_ids
    });
  }

}
