import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Authprovider } from './../../providers/authprovider';
import { Doctorprovider } from './../../providers/doctorprovider';

/**
 * Generated class for the Register page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class Register {
  defaultImage = "assets/vendor/iconProfilePic.png";
  profileImage;
  register_path: string = "doctors/register";
  specialisationIds: any[] = [];
  specialisation_path: string =  "specialisation_list";
  signup_form: FormGroup;
  email_regex = /^(?:(?:[\w`~!#$%^&*\-=+;:{}'|,?\/]+(?:(?:\.(?:"(?:\\?[\w`~!#$%^&*\-=+;:{}'|,?\/\.()<>\[\] @]|\\"|\\\\)*"|[\w`~!#$%^&*\-=+;:{}'|,?\/]+))*\.[\w`~!#$%^&*\-=+;:{}'|,?\/]+)?)|(?:"(?:\\?[\w`~!#$%^&*\-=+;:{}'|,?\/\.()<>\[\] @]|\\"|\\\\)+"))@(?:[a-zA-Z\d\-]+(?:\.[a-zA-Z\d\-]+)*|\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\])$/;

  user = {
    doctor: {
      'first_name': null,
      'last_name' : null,
      'email' : null,
      'password' : null,
      'phone_attributes' : {'number': ''},
      'specialisation_ids' : null
    }
  };

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private fb: FormBuilder,
              private authProvider: Authprovider,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController,
              private camera: Camera,
              private dp: Doctorprovider) {

    this.signup_form = fb.group({
      // This will validate the input fields
      first_name: [null, Validators.required],
      last_name: [null, Validators.required],
      email: [null, Validators.compose([Validators.required, Validators.pattern(this.email_regex)])],
      password: [null, Validators.compose([Validators.required, Validators.minLength(8)])],
      confirm_password: [null, Validators.compose([Validators.required, Validators.minLength(8)])],
      number: [null, Validators.required],
      specialisation_ids: [null, Validators.required]
    }, {
      validator: this.passwordMatch('password', 'confirm_password')
    });
  }

  passwordMatch(password:string, confirm_password:string) {
    return (group: FormGroup): {[key: string]:any} => {
      let pwd1 = group.controls[password];
      let pwd2 = group.controls[confirm_password];

      if((pwd1.touched || pwd2.touched)  && (pwd1.value !== pwd2.value)) {
        return {passwords_mismatch: true} ;
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Register');
    this.get_specialisation_ids();
  }

  registerUser(path, user) {
    //Set the please wait loader
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();

    this.authProvider.register(path, user).subscribe((response) => {
      //Dismiss please wait Loader
      loader.dismiss();

      // Display success message on successful registration
      let alert = this.alertCtrl.create({
          title: "Account created",
          message: "Click ok to login.",
          buttons: [{text: "ok", handler: ()=> {
            this.goBacktoPreviousPage();
          }}]
        });
      alert.present();
    },
    (err) => {
      //Dismiss please wait Loader
      loader.dismiss();

      // Display error message on successful registration
      let alert = this.alertCtrl.create({title: "Error", buttons: ["ok"]});
      alert.present();
    });
  }

  get_data(form) {
    // Format user inputs
    this.user.doctor["first_name"] = form.first_name;
    this.user.doctor["email"] = form.email;
    this.user.doctor["last_name"] = form.last_name;
    this.user.doctor["password"] = form.password;
    this.user.doctor["specialisation_ids"] = form.specialisation_ids;
    this.user.doctor.phone_attributes['number'] = form.number;

    this.registerUser(this.register_path, this.user);

  }

  get_specialisation_ids() {
    this.dp.getSpecialisations(this.specialisation_path).subscribe((res) => {

      this.specialisationIds = res.specialisation;
      console.log(this.specialisationIds);

    });

  }

// To take the user back to sign in screen
  goBacktoPreviousPage() {
    this.navCtrl.pop();
  }

 accessGallery() {
   this.camera.getPicture({
     sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
     destinationType: this.camera.DestinationType.DATA_URL
    }).then((imageData) => {
      this.profileImage = 'data:image/jpeg;base64,'+imageData;
     }, (err) => {
      console.log(err);
    });
  }

}
