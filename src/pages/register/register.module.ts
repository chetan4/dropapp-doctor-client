import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Register } from './register';

@NgModule({
  declarations: [
    Register,
  ],
  imports: [
    IonicPageModule.forChild(Register),
  ],
  exports: [
    Register
  ],
  providers: [
    Camera
  ]
})
export class RegisterModule {}
