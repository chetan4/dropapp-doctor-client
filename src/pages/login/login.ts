import { ChangeDetectorRef, ChangeDetectionStrategy, Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { HomePage } from './../home/home';
import { Register } from '../register/register';
import { ForgotPassword } from './../forgot-password/forgot-password';
import { Authprovider } from './../../providers/authprovider';
import { TokenManager } from './../../providers/token-manager';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Login {
  user = {
    doctor: {
      'email' : null,
      'password' : null
    }
  };
  forgot_password: any;
  register: any;
  home: any;
  login_path: string = "doctors/login";
  email_regex = /^(?:(?:[\w`~!#$%^&*\-=+;:{}'|,?\/]+(?:(?:\.(?:"(?:\\?[\w`~!#$%^&*\-=+;:{}'|,?\/\.()<>\[\] @]|\\"|\\\\)*"|[\w`~!#$%^&*\-=+;:{}'|,?\/]+))*\.[\w`~!#$%^&*\-=+;:{}'|,?\/]+)?)|(?:"(?:\\?[\w`~!#$%^&*\-=+;:{}'|,?\/\.()<>\[\] @]|\\"|\\\\)+"))@(?:[a-zA-Z\d\-]+(?:\.[a-zA-Z\d\-]+)*|\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\])$/;
  signin_form: FormGroup;
  is_authenticated: boolean = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private fb: FormBuilder,
              private authProvider: Authprovider,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController,
              private tokenManager: TokenManager,
              public changeDetector: ChangeDetectorRef) {

    this.register = Register;
    this.forgot_password = ForgotPassword;
    this.home = HomePage;

    this.signin_form = fb.group({
      email: ["bulah.e@example.com", Validators.compose([Validators.required, Validators.pattern(this.email_regex)])],
      password: ["Test1234", Validators.required],
    })

  }

  get_data(form) {

    // Format user inputs
    this.user.doctor["email"] = form.email;
    this.user.doctor["password"] = form.password;

    this.loginUser(this.login_path, this.user);
  }

  loginUser(path, user) {
    //Set the please wait loader
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();

    this.authProvider.login(path, user).subscribe((response) => {
      //Dismiss please wait Loader
      loader.dismiss();

      // Login User logic
      this.tokenManager.store_token(response.auth_token);
      this.check_authentication();

    },
    (err) => {
      //Dismiss please wait Loader
      loader.dismiss();
      // Display error message on successful registration
      let errors = JSON.parse(err._body).errors;
      let alert = this.alertCtrl.create({
        title: "Error",
        message: errors.doctor[0],
        buttons: ["ok"]});
      alert.present();
    });

  }

  ionViewDidLoad() {


  }

  check_authentication() {
    this.tokenManager.check_token().subscribe((response) => {
      if(response) {
        this.navCtrl.setRoot(HomePage);
      } else {
        this.navCtrl.setRoot(Login);
      }
      this.navCtrl.popToRoot();
    });

  }



}
