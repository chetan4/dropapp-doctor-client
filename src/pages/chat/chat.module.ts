import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { Chat } from './chat';
import { AngularFireDatabase } from 'angularfire2/database';
import { ChatService } from '../../providers/chatprovider';

@NgModule({
  declarations: [
    Chat,
  ],
  imports: [
    IonicPageModule.forChild(Chat),
  ],
  exports: [],
  providers: [ChatService, AngularFireDatabase]
})
export class ChatModule {}
