import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { ChatService } from '../../providers/chatprovider';

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html'
})

export class Chat {
  chats: FirebaseListObservable<any[]>;
  chat_form: FormGroup;
  chat = {
    fid: 4,
    ftype: 'Doctor',
    msg: 'Test',
    tid: 13,
    ts: null,
    ttype: 'Patient',
    type: 'text'
  }
  // chat= {
  //   message: null,
  //   chat_type: "text",
  //   patient_id: 13
  // }
  @ViewChild(Content) content: Content;

  constructor(private chatService: ChatService,
              private navCtrl: NavController,
              public navParams: NavParams,
              private fb: FormBuilder,
              private afd: AngularFireDatabase) {

                this.chats = this.afd.list("/chats/d-4-p-13");
                this.chat_form = this.fb.group({
                  message: [null, Validators.required]
                });

              }

  ionViewWillEnter() {
    let that = this;
    // setTimeout(function() {
    that.content.scrollToBottom(0);
    // }, 1500);
  }

callFunction() {
  this.content.scrollToBottom(0);
}



  createMessage(form) {
    console.log(Date.now());
    // let token = localStorage.getItem('auth_token');
    // this.chat.message = form.message;

    // this.chatService.create(token , {chat: this.chat}).subscribe((res) => {
    //   this.chat.message = '';
    //
    //   console.log(Date.now);
    // });
    if(form.message){
      this.chat.msg = form.message;
      this.chat.ts = Date.now();
      this.chats.push(this.chat);

      console.log(Date.now());
      this.chat.msg = '';
      this.chat_form.reset();
    }

  }

}
